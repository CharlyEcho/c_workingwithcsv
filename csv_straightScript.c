#include "stdio.h"
#include "stdlib.h"

/************************************************************************************************************************
*                                                                                                                       *
*  EINLESEN, BEARBEITEN UND AUSGEBEN EINER CSV-DATEI: AUTOMATISCHE ZEILEN UND SPALTENERKENNUNG SOWIE SPEICHERZUWEISUNG  *                                                               *
*                                                                                                                       *
************************************************************************************************************************/

int main(int argc, char *argv[]){

    /*** ALLGEMEINER FUNKTIONSTEST ***/
//    printf("hello world");

    /*** VARIABLEN***/

    // DATEI
    FILE * filePointer;
    char fileName[25] = "testdaten.txt";

    // INHALT AUFNEHMEN UND ZERLEGEN
    int fileContentAmountOfChars = 1000;
    char fileContent[1000] = "";
    char delimiter[] = ",;";
    char *ptr;

    // VORABINFORMATION
    int numberOfRows = 5;
    int numberOfColums = 3;
    int numberOfFields = 15;
    int numberOfChars = 10;

    // INHALT ABSPEICHERN
    char content[numberOfRows][numberOfColums][numberOfChars];
    char contentChanged[numberOfRows][numberOfColums][numberOfChars];

    // INHALT VERMESSEN
    int stringLength[numberOfRows][numberOfColums];
    for(int i=0; i<numberOfRows; i++)
    {
        for (int j=0; j<numberOfColums; j++)
        {
            stringLength[i][j] = 0;
//            printf("Tatsaechlihce Laenge des Strings in Reihe %i und Spalte %i ist mit %i intitialisiert.\n", i, j, stringLength[i][j]);
        }
    }
    int stringLengthMax[numberOfColums];
    for(int i=0; i<numberOfColums; i++)
    {
        stringLengthMax[i] = 0;
//        printf("Maximale Laenge der Strings in Spalte %i ist mit %i initialisiert.\n", i, stringLengthMax[i]);
    }
    int stringLengthDiff[numberOfRows][numberOfColums];
    for(int i=0; i<numberOfRows; i++)
    {
        for (int j=0; j<numberOfColums; j++)
        {
            stringLengthDiff[i][j] = 0;
//            printf("Differenz der Laenge des Strings in Reihe %i und Spalte %i ist mit %i intitialisiert.\n", i, j, stringLength[i][j]);
        }
    }

    // ZAEHLER
    int countOfRows = 0;
    int countOfColumns = 0;
    int countOfFields  = 0;

    /*** OEFFNEN DER DATEI ***/

    /* DATEI �FFNEN*/
    filePointer = fopen(fileName, "r");

    /* �FFNUNG �BERPR�FEN*/
    if(filePointer == NULL)
    {
        perror("Die Datei konnte NICHT geoeffnet werden.\n");
//        printf("Die '%s' Datei konnte NICHT geoeffnet werden.\n", fileName);
        exit(EXIT_FAILURE);
    }
//    else
//    {
//        printf("Datei '%s' konnte erfolgreich geoeffnet werden.\n", fileName);
//    }

/************************************************************************************************************************
*   PHASE 1. BEMESSEN DER DATEN                                                                                         *
************************************************************************************************************************/

    printf("\nPHASE 1: BEMESSEN DER DATEN\n");

    /* INHALT AUSLESEN AUSGEBEN*/
//    printf("Datei '%s' hat folgenden Inhalt:\n", fileName);
    if(fgets(fileContent, fileContentAmountOfChars, filePointer) != NULL)
    {
//        printf("%s", fileContent);
//        puts(fileContent);

        // INHALT ZERLEGEN
        ptr = strtok(fileContent, delimiter);
        while (ptr != NULL)
        {
            // INHALT TESTWEISE AUSGEBEN
//            printf("Textabschnitt des Inhaltes gefunden: %s\n", ptr);

            // ZAEHLER AKTUALISIEREN
            if(countOfFields == 0) countOfRows++;      // NUR BEI ERSTER ITERATION
            countOfColumns++;           // SPALTE VON 0 auf 1
            countOfFields++;            // FELD VON 0 AUF 1

            // ZEILENUMBRUCH BER�CKSICHTIGEN
            if(countOfColumns > numberOfColums)
            {
                countOfColumns=1;       // SPALTE ZUR�CK SETZEN
                countOfRows++;          // REIHE WEITERZ�HLEN
            }

            // INHALT IN ARRAY KOPIEREN
//            strcpy(content[countOfRows-1][countOfColumns-1], ptr);
//            puts(content[countOfRows-1][countOfColumns-1]);

            // INHALT BEMESSEN
            stringLength[countOfRows-1][countOfColumns-1] =  strlen(ptr);
            if(stringLength[countOfRows-1][countOfColumns-1] > stringLengthMax[countOfColumns-1])
            {
                stringLengthMax[countOfColumns-1] = stringLength[countOfRows-1][countOfColumns-1];
            }

            // WEITERES ZERLEGEN DES RESTLICHEN INHALTES
            ptr = strtok(NULL, delimiter);
        }
    }
    /* SPEICHER ZUWEISEN*/
    // TBD

/************************************************************************************************************************
*   PHASE 2 = AUFNEHMEN DER DATEN                                                                                       *
************************************************************************************************************************/

    printf("\nPHASE 2 = AUFNEHMEN DER DATEN\n");

    /* ZEIGER DES DATENSTROMS ZUR�CKSETZTEN */
    rewind(filePointer);

    /* ZAEHLER ZUR�CKSETZEN */
    countOfRows = 0;
    countOfColumns = 0;
    countOfFields  = 0;

    /* DATENSTROM ERNEUT AUSLESEN */
    if(fgets(fileContent, fileContentAmountOfChars, filePointer) != NULL)
    {
        ptr = strtok(fileContent, delimiter);
        while (ptr != NULL)
        {
            // INHALT TESTWEISE AUSGEBEN
            printf("Textabschnitt des Inhaltes gefunden: %s\n", ptr);

            // ZAEHLER AKTUALISIEREN
            if(countOfFields == 0) countOfRows++;      // NUR BEI ERSTER ITERATION
            countOfColumns++;           // SPALTE VON 0 auf 1
            countOfFields++;            // FELD VON 0 AUF 1

            // ZEILENUMBRUCH BER�CKSICHTIGEN
            if(countOfColumns > numberOfColums)
            {
                countOfColumns=1;       // SPALTE ZUR�CK SETZEN
                countOfRows++;          // REIHE WEITERZ�HLEN
            }

            // INHALT IN ARRAY KOPIEREN
            strcpy(content[countOfRows-1][countOfColumns-1], ptr);
//            puts(content[countOfRows-1][countOfColumns-1]);

            // INHALT BEMESSEN
            stringLength[countOfRows-1][countOfColumns-1] =  strlen(ptr);
            if(stringLength[countOfRows-1][countOfColumns-1] > stringLengthMax[countOfColumns-1])
            {
                stringLengthMax[countOfColumns-1] = stringLength[countOfRows-1][countOfColumns-1];
            }

            // WEITERES ZERLEGEN DES RESTLICHEN INHALTES
            ptr = strtok(NULL, delimiter);
        }
     }

/************************************************************************************************************************
*   PHASE 3: ANPASSEN DER DATEN                                                                                         *
************************************************************************************************************************/

    printf("\nPHASE 3: ANPASSEN DER DATEN\n");

    for(int i=0; i<numberOfRows; i++)
    {
        for(int j=0; j<numberOfColums; j++)
        {

            /*  KOPIEREN DES ORIGINALEN INHALT */
            strcpy(contentChanged[i][j], content[i][j]);
            printf("String: Zeile %i, Spalte %i, This = %i, Max = %i\n", i, j, stringLength[i][j], stringLengthMax[j]);

            /* AENDERUNGEN DES KOPIERTEN INHALTES */
            if(stringLength[i][j]<stringLengthMax[j])
            {
                // DIFFERENZ ERMITTELN
                stringLengthDiff[i][j] = stringLengthMax[j] - stringLength[i][j];
                printf("String: Zeile %i, Spalte %i --> Differenz ermittelt!\n", i, j);

                // LEEREN STRING MIT FUELLZEICHEN BAUEN
                char fillUpChar[1] = "x";
                char fillUpString[5] = "";
                for (int k=0; k<stringLengthDiff[i][j]; k++)
                {
                    strcat(fillUpString, "x");
                    printf("String: Zeile %i, Spalte %i --> Erweiterung vorbereitet!\n", i, j);
                }

                /* AENDERUNG AM KOPIERTEN INHALT VORNEHMEN*/
                strcat(contentChanged[i][j], fillUpString);
                printf("String: Zeile %i, Spalte %i --> Erweiterung durchgefuehrt!\n", i, j);
                printf("String: Zeile %i, Spalte %i, This = %i, Max = %i\n", i, j, strlen(contentChanged[i][j]), stringLengthMax[j]);
            }
        }
    }

/***********************************************************************************************************************/

    /* DATEI SCHLIESSEN */
    fclose(filePointer);
    free(content);
    free(stringLengthMax);
    free(stringLength);


    return 0;
};
