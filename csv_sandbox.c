/************************************************************************************************
*                                                                                               *
*                                                                                               *
*                      E I N L E S E N   V O N    C S V - D A T E I E N                         *
*                                                                                               *
*                                                                                               *
************************************************************************************************/


/************************************************************************************************
*                                                                                               *
*                                       PRÄPROZESSOR                                            *
*                                                                                               *
************************************************************************************************/

/*** INCLUDE  ***/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <locale.h>

/*** DEFINE ***/

/* ARRAYS OF CHARS */
#define MIN_FIELD_ELEMENTS      20
#define MID_FIELD_ELEMENTS      50
#define MAX_FIELD_ELEMETNS      100

/* BUFFER AND ALLOCATION */
#define MAX_BUFFER_FIELD        64      // WILLKUERLICH GESETZT
#define MAX_BUFFER_LINE         256     // WILLKUERLICH GESETZT >> IM INTERVALL [0-255] >>> BEACHTE: INCL. '\0'
#define MAX_BUFFER_FILE         1024    // WILLKUERLICH GESETZT >> BIS 1024  >>  BEI BEDARF ERHOEHEN
#define MAX_BUFFER_CHARS        266124  // MAX_BUFFER_FILE * MAX_BUFFER_LINE 

/************************************************************************************************
*                                                                                               *
*                                       VARIABLEN                                              *
*                                                                                               *
************************************************************************************************/

/*** HEADER ***/

/* TITEL */
char titel[MID_FIELD_ELEMENTS];
char untertitel[MAX_FIELD_ELEMETNS];
char titelzusatz[MID_FIELD_ELEMENTS];
char autor[MID_FIELD_ELEMENTS];

/* TIMESTAMP*/
time_t rawtime;
struct tm *info;
char buffer[MAX_BUFFER_LINE];       // !!! >> OBSOLETE ??? >> NICHT IN TIME GENUTZT

/*** DATEI ***/

/* DATEI*/
FILE *datenstrom;
char datei[MAX_FIELD_ELEMETNS];

/* DATEN */
int zeichen;

/* META_DATEN */

// DELIMITER
const char DELIMITER_FIELD[2]        = ",";  
const char DELIMITER_LINE[2]         = "\r";
const char DELIMITER_STRING[2]       = "\0";  // BINAERE NULL >> NULL

// GRENZEN
int anzahlDelimiter;
int anzahlZeilen;
int anzahlSpalten;
int anzahlZeichen;

// ZAEHLER
int zaehlerSpalten    = 0;
int zaehlerSpaltenMax = 0;
int zaehlerZeilen     = 0;
int zaehlerDelimiter  = 0;
int zaehlerToken      = 0;
int zaehlerZeichen    = 0;

// INHALT
char datenZeile[MAX_BUFFER_FILE];   // sind 1024
char bufferZeile;

// STRUKTUR DEFINIEREN
struct immos{
    char *street;
    char *city;
    char *zip;
    char *state;
    int  beds;
    int  baths;
    double sq__ft;
    char *type;
    char *sale_date;
    double price;
    char *latitude;
    char *longitude;
};

//STRUKTUR INITIALISIEREN
struct immos immo[MAX_BUFFER_FILE];

/************************************************************************************************
*                                                                                               *
*                                           PROTOTYPEN                                          *
*                                                                                               *
************************************************************************************************/


/************************************************************************************************
*                                     ALLGEMEINE FORMATIERUNG                                   *
************************************************************************************************/

// TRENNLINIE
void printSeparation(){
        printf("\n\n-------------------------------------------------------------------------------------------------\n\n");
}

/************************************************************************************************
*                                        HEADER FUNKTIONEN                                      *
************************************************************************************************/

/*** VORBEREITUNG DES HEADERS***/
// ERMITTLUNG UND FESTLEGUNG DES TITELS
void setTitle(){
    strcpy(titel, "E I N L E S E N   V O N    C S V - D A T E I E N\n");
    strcpy(untertitel, "EINLESEN UEBER FUNKTION 'FOPEN' UND 'FSEEK'\n");
    strcpy(titelzusatz, "Lesen im Textmodus 'rt'");
};
// ERSTELLER FESTLEGEN
void setAutor(){
    strcpy(autor, "C.Eick");
};
// ERMITTLUNG UND FESTELGUNG DER ZEIT
void setTime(){
    time( &rawtime );
    info = localtime( &rawtime );
};
// ZUSAMMENSETZEN DES HEADERS
void setHeader(){
    setTitle();
    setAutor();
    setTime();
};
/*** ANZEIGEN DES HEADERS ***/
// AUSDRUCK DES TITELS
void printTitle(){
    printf("\nTitel:\t\t\t%s\n", titel);
    printSeparation();
    printf("\nUntertitel:\t%s\n", untertitel);
    printf("\nTitelzusatz:\t%s\n", titelzusatz);
};
// AUSDRUCK DES ERSTELLERS
void printAutor(){
    printf("\nAutor:\t\t%s\n", autor);
}
// AUSFRUCK DER ZEIT
void printTime(){
    printf("\nDate:\t\t%s\n", asctime(info));
    //printf("\nFile:\t%s\n",argv[0]);
};
// AUSDRUCK DES GESAMTEN HEADERS
void printHeader(){
    printTitle();
    printAutor();
    printTime();
    printSeparation();
};

/************************************************************************************************
*                                ALLGEMEINE FILE FUNKTIONEN                                     *
************************************************************************************************/

// DATEI FESTLEGEN
void fileDeclarePath(){
    //strcpy(datei, "C:\\Users\\ceick\\Documents\\Sacramentorealestatetransactions.csv");
    strcpy(datei, "Sacramentorealestatetransactions.csv");
    printf("\nDateiname: \t%s\n", datei);
}

// DATEI ÖFFNEN
void fileOpen(){
    // DATENSTROM ERZEUGEN
    datenstrom = fopen(datei,"rt");
    // DATENSTROM ÜBERPRÜFEN
    if(datenstrom != NULL) {
        /* RÜCKMELDUNG AN DEN NUTZER*/
        printf("\nOeffnen: \t\tERFOLGREICH!\n");
        printSeparation();
    }
    else {
        printf("\nOeffnen: \t\tNICHT ERFOLGREICH!\n");
        exit(1);
    }
}
// ERGEBNIS DES AUSLESENS DRUCKEN
void printResult(){
    printf("GELESEN\t\tANZAHL\n");
    printf("-----------------------\n");
    printf("Spalten\t\t%i\n", anzahlSpalten);
    printf("Zeilen\t\t%i\n",anzahlZeilen);
    printf("Zeichen\t\t%i\n",anzahlZeichen);
    printSeparation();
}
// DATEI SCHLIESSEN
void fileClose(){
    fclose(datenstrom);
}
//SPEICHER FREIGEBEN
void fileFree(){
    free(datenstrom);
}

/************************************************************************************************
*                                FILE FUNKTIONEN ZUR BEARBEITUNG                                *
************************************************************************************************/

/*-------------------------EINLESEN UND DURCHLAUFEND ÜBER CLI AUSGEBEN ------------------------*/

/* 
    BESCHREIBUNG:   FileGetChar->PutChar
    BEMERKUNG:      KEINE TRENNUNG NACH ZEILEN SPALTEN ODER FELDERN
    STATUS:         LAUEFT

*/

void fileReadCharAndPrintToCLI(){
        if(datenstrom != NULL) {
                do{
                    zeichen = fgetc(datenstrom);
                    putchar(zeichen);               // putchar()->  Ausgabe des zuvor eingelesenen Zeichens
                    anzahlZeichen++;
                }while(!feof(datenstrom));           // feof     ->  Pr�fen auf Ende der Zeichenkette
                printSeparation();
                printf("gelesene Zeichen:\t %i", anzahlZeichen);
                printSeparation();
        }
}

/*---------------------EINLESEN UND ÜBER ZEILENWEISE ÜBER CLI AUSGEBEN ------------------------*/
/* 
    BESCHREIBUNG:   FileGetChar->PruefenAufControlzeichen->PutChar
    BEMERKUNG:      TRENNUNG           ZEILEN
                    KEINE TRENNUNG     SPALTEN ODER FELDERN
    STATUS:         LAUEFT

*/

void fileReadCharCheckLinesAndPrintCLI(){
            if(datenstrom != NULL) {
                do{
                    zeichen = fgetc(datenstrom);
                    // PRÜFEN AUF STEUERZEICHEN UND ZEICLENUMBRUCH
                    if(iscntrl(zeichen)){
                        anzahlZeilen++;
                        printf("\n");
                    }
                    // AUSGABE ALLER ZEICHEN IN CONSOLE
                    else{
                        putchar(zeichen);   // putchar()-> Ausgabe des zuvor eingelesenen Zeichens
                    }
                    // HOCHZAEHLEN DER CHARS
                     anzahlZeichen++;
                }while(!feof(datenstrom));  // feof     -> Pruefen auf Ende der Zeichenkette
                printSeparation();
        }
}

/*-----------------CHARWEISES EINLESEN ZEILENWEISE, FORMATIERTE CLI AUSGEBEN-------------------*/
/* 
    BESCHREIBUNG:   FileGetChar->DO-While->IF,ELSEIF->ZaehlerAufEigentlichAlles->PutChar
    BEMERKUNG:      TRENNUNG        >>  ZEICHEN->ZEILE->SPALTE
                    KEINE TRENNUNG  >>  ZEICHEN
    STATUS:         LAEUFT
*/

void fileReadCharCheckLinesPlusCommaAndPrintCLI(){

    // GRENZWERETE SETZTEN
    anzahlSpalten   =   12;
    anzahlZeilen    =   1000;       
    anzahlDelimiter =   11;         // TRENNZEICHEN INNERHALB DER ZEILE >> HIER BEKANNT >> ',' == 15
    anzahlZeichen   =   0;

    // ZAEHLER AUF NULL SETZEN      >> SOLLTEN SEIT DEKLARATION/INITIALISIEREN NULL SEIN >> IST JUST IN CASE
    zaehlerSpalten      =   0;
    zaehlerZeilen       =   0;
    zaehlerToken        =   0;
    zaehlerDelimiter    =   0;
    zaehlerZeichen      =   0;

    // TEMPORAERE ZAEHLER
    zaehlerSpaltenMax   =   0;      // HILFE ZUR ERMITTLUNG DER ANZAHL VORHANDENER SPALTEN (ROWS)

    // DELIMITER
    char delimiter  = ',';
    char endOfLine  = '\r';
    char endOfFile  = '\0';

    if(datenstrom != NULL) {

        do{
            // EINZELNES CHAR ÜBERGEBEN
            zeichen = fgetc(datenstrom);
 
            // SCHREIBEN IN CONSOLE
            putchar(zeichen);

            // HOCHZAEHLERN DER CHARS
            zaehlerZeichen++;

            // PRUEFEN AUF DATEIENDE
            if(zeichen=='\0' && zaehlerDelimiter==11){      // HIER NUTZLOS, DA PRUEFUNG SCHON FORAB IN IF
                printf("\nEND OF FILE");    // WENN EOF, DANN KEIN LOOP
            }
            // PRUEFEN AUF ZEILENENDE
            else if(zeichen=='\r' && zaehlerDelimiter==11){
                zaehlerZeilen++;
                zaehlerSpalten      = 0;
                zaehlerDelimiter    = 0;
                //printf("\tEND OF LINE\n");
            }
            // PRUEFEN AUF SPALTENENDE
            else if (zeichen==',' && zaehlerDelimiter<11){
                zaehlerDelimiter++;
                zaehlerSpalten++;
                if(zaehlerSpalten>zaehlerSpaltenMax){zaehlerSpaltenMax=zaehlerSpalten;}
                printf("\t\t");
            }
        }// ÜBERPRÜFUNG DER SCHLEIFENBEDINGUNG, HIER: WDH. WENN NICHT 'END OF FILE'
        while(!feof(datenstrom));

        // RESULTAT DER ZAEHLUNG
        anzahlSpalten   =   zaehlerSpaltenMax+1; // Spalten zaehlen mit Delimiter UND Spalten pro Zeile Delimiter plus 1
        anzahlZeilen    =   zaehlerZeilen;
        anzahlZeichen   =   zaehlerZeichen;   

        // FORMATIERUNG ABSCHLUSS ASL OPTISCHE ABGRENZUNG
        printSeparation();
    }
}

/*-----------------EINLESEN UND ZEILENWEISE FORMATIERTE AUSGABE ÜBER CLI------------------*/
/* 
    BESCHREIBUNG:   FileGetString->Struct->While>PutChar
    BEMERKUNG:      TRENNUNG        >>  ZEILEN, SPALTEN
                    KEINE TRENNUNG  >>  ZEICHEN
    STATUS:         UNDER CONSTRUCTION
*/

// https://stackoverflow.com/questions/20212714/reading-a-csv-file-into-struct-array

void fileReadGetStringToArrayOfStruct(){

    // LOKALE POINTER DEKLARIEREN >> FUER DATENSTROM >> SPEICHER ZUWEISEN
    char *bufFilePt         = (char *)malloc(MAX_BUFFER_CHARS);     // SPEICHERPLATZ >> DATEI (GESAMTER STRING)
    char *tmpFilePt         = (char *)malloc(MAX_BUFFER_CHARS);     // TEMP SPEICHER >> DATEI
    char *tmpLinePt         = (char *)malloc(MAX_BUFFER_LINE);      // TEMP SPEICHER >> ZEILE
    char *tmpFieldPt        = (char *)malloc(MAX_BUFFER_FIELD);     // TEMP SPEICHER >> FELD
    char *tmpLastTokenPt    = (char *)malloc(MAX_BUFFER_LINE);     // TEMP SPEICHER >> LETZTES ZEICHEN

    // LOKALE INHALTE DEKLARIEREN
    char tmpFileCont[MAX_BUFFER_CHARS];
    char tmpLineCont[MAX_BUFFER_LINE];  
    char tmpFieldCont[MAX_BUFFER_FIELD];    

    // ÜBERPRUEFUNG DES FREIGEGEBENEN SPEICHERS >> BUFFER
    if (bufFilePt == NULL || tmpLinePt == NULL || tmpFieldPt == NULL){
        printf("PRINTF >> FEHLER: Kein Speicher für BUFFER UND TEMPS verfuegbar!");
        perror("PERROR >> FEHLER: Kein Speicher fuer BUF verfuegbar!");
        exit(-1);
    }

    // TEST AUF EMPFANG ON DATEN
    if(datenstrom!=NULL){
        
        //LOKALE ZAEHLER DER EINTRAEGE
        zaehlerSpalten      =   0;
        zaehlerSpaltenMax   =   0;
        zaehlerZeilen       =   0;
        zaehlerDelimiter    =   0;
        zaehlerZeichen      =   0;

        // STRING VON DATENSTROM LESEN
        while(fgets(bufFilePt, MAX_BUFFER_CHARS, datenstrom))
        {
            /*** ZEILENWEISES AUSGEBEN MIT STRTOK >> LAEUFT =) ***/
            /*
            tmpFilePt   = strdup(bufFilePt);
            tmpLinePt   = strtok(tmpFilePt, "\r");
            while(tmpLinePt)
            {
                tmpLinePt = strtok(NULL, "\r");
            }
            */
           
            /*** ZEILENWEISES AUSGEBEN MIT STRTOK >> LAEUFT NICHT =( ***/
            // UEBERGABE DES BUFFERS AND TEMP
            tmpFilePt   = strdup(bufFilePt);                    // DUPLICATE FILE
            tmpLinePt   = strtok(tmpFilePt,DELIMITER_LINE);     // TOKENIZE FILE TO LINE

            // PRUEFUNG: BEI STRINGENDE >> KEINE DELIMITERS AUF IN DATEI
            if (tmpLinePt == NULL)
            {
                printf("%s\n", tmpLinePt);
                puts("NO LINE_DELIMITERS FOUND");
            }
            // PRUEFUNG: KEIN STRINGENDE >> WEITERE AUFGLIEDERUNG
            else
            {   
                // AEUSSERE SCHLEIFE >> ZERLEGUNG DES FILES IN ZEILEN
                while(tmpLinePt)                                    
                {
                    // UEBERGABE INNERHALB DER TEMP
                    //tmpFieldPt   = strdup(tmpLinePt);                  // DUPLICATE LINE
                    tmpLinePt   = strtok(NULL,DELIMITER_LINE);  // TOKENIZE LINE TO FIELD

                    // PRUEFUNG: BEI STRINGENDE >> KEINE DELIMITERS IN LINES 
                    if (tmpLinePt == NULL)
                    {
                        printf("%s\n", bufFilePt);
                        puts("NO FIELD_DELIMITERS FOUND");
                    }
                    else
                    {
                        // INNERE SCHLEIFE >> ZERLEGUNG DES FILES IN ZEILEN
                        while(tmpLinePt)
                        {
                            //printf("%s\t", tmpFieldPt);
                            tmpLinePt = strtok(NULL, DELIMITER_FIELD);
                        }
                    }
                }
            }
            
            /*
            tmpLinePt = strrchr(bufFilePt, (int)DELIMITER_LINE);
            if (tmpLinePt != 0){
                printf("%s\n",tmpLinePt+1);
            }
            */
            /*
            tmpLinePt = strtok(bufFilePt, DELIMITER_LINE);
            tmpLastTokenPt = tmpLinePt;
            for(; (tmpLinePt = strtok(NULL, DELIMITER_LINE)) != NULL; tmpLastTokenPt = tmpLinePt);
            //while (tmpLinePt != NULL) {
            //    printf("%s,\n",tmpLastTokenPt);
            }
            */

            /** ZUWEISUNG TEMPDATEIEN ALS PINTER UND VARIABLEN **/

            /* NICE TRY -> BUT CAUGHT IN LOOP
            tmpFilePt = strdup(bufFilePt);                      // KOPIE DES DATENSTROMS IN TEMP
            while(tmpFilePt != NULL && zaehlerSpalten < 13)
            {
                tmpLinePt = strdup(tmpFilePt);
                tmpLinePt = strtok(tmpLinePt, DELIMITER_LINE);

                ////>> UNABLE TO SET LAST CHAR OF STRING TO '\0' >> FOR END OF STRING
                //if((tmpLinePt[(strlen(tmpLinePt)-1)])==DELIMITER_LINE)
                {
                //   (tmpLinePt[(strlen(tmpLinePt)-1)])=DELIMITER_STRING;          
                }

                while(tmpLinePt != NULL)
                {
                    tmpFieldPt = strdup(tmpLinePt);
                    tmpFieldPt = strtok(NULL, DELIMITER_FIELD);
                    zaehlerSpalten++;
                    if(zaehlerSpalten>zaehlerSpaltenMax){zaehlerSpaltenMax=zaehlerSpalten;};
                } 
            }
            */

            //tmpLinePt = strtok(tmpFilePt, DELIMITER_LINE);
            //tmpLinePt = strdup(tmpFilePt);                      // ZEILE HERAUSFILTERN >> KOPIEREN
            //tmpLinePt = strtok(tmpLinePt, DELIMITER_LINE);      // ZEILE HERAUSFILTERN >> BEGRENZEN

            // AEUSSERER LOOP >> ZEILENWEISE
            /*
            //while(tmpLinePt != NULL){
                tmpLinePt = strtok(tmpFilePt, DELIMITER_LINE);
                zaehlerSpalten = 0;                         // Zaehler Zurücksetzten
                zaehlerZeilen++;                            // ANZAHL DER ZEILEN HOCHZAEHLEN
                tmpFieldPt = strdup(tmpLinePt);
                 
                // INNERER LOOP >> FELDERWEISE
                while (tmpFieldPt != DELIMITER_FIELD && zaehlerSpalten < 13){
                    zaehlerSpalten++;                           // ANZAHL DER FELDER HOCHZAEHLEN
                    if(zaehlerSpalten>zaehlerSpaltenMax){zaehlerSpaltenMax=zaehlerSpalten;}          // HOECHST GEMESSENE ANZAHL
                    tmpFieldPt = strtok(NULL, DELIMITER_FIELD);   // WEITERE ZERTEILUNG IN FELDER
                    //printf("%s\t",tmpFieldPt);                  // AUSGABE DER FELDER
                };
            };

            /*
            // STRING ZERTEILEN >>  ABFRAGE DES DELIMITERS UND ZUORDNUNG ZU STRUKTUR-BEREICH
            tmpLinePt = strtok(bufFilePt, "\r");
            immo[zaehlerZeilen].street          = strdup(tmpFieldPt);

            tmpLinePt = strtok(bufFilePt, ",");
            immo[zaehlerZeilen].city            = strdup(tmpFieldPt);

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].zip             = strdup(tmpFieldPt);

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].state           = strdup(tmpFieldPt);

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].beds            = atoi(strdup(tmpFieldPt));

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].baths           = atoi(strdup(tmpFieldPt));

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].sq__ft          = atof(strdup(tmpFieldPt));

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].type            = strdup(tmpFieldPt);

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].sale_date       = strdup(tmpFieldPt);

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].price           = atoi(strdup(tmpFieldPt));

            tmpLinePt = strtok(NULL, ",");
            immo[zaehlerZeilen].latitude        = strdup(tmpFieldPt);

            tmpLinePt  = strtok(NULL, ",");
            immo[zaehlerZeilen].longitude       = strdup(tmpFieldPt);

            printf("index %i,\t%c, \t%c, \t%c, \t%c. \t%f\n",
                zaehlerZeichen,
                immo[zaehlerZeilen].street,
                immo[zaehlerZeilen].city,
                immo[zaehlerZeilen].zip,
                immo[zaehlerZeilen].state,
                immo[zaehlerZeilen].price);
            */
        }

        //SPEICHER WIEDER FREIGEBEN
        free(bufFilePt);
        free(tmpFilePt);
        free(tmpLinePt);
        free(tmpFieldPt); 

        // RESULTAT DER ZAEHLUNG
        anzahlSpalten   =   zaehlerSpalten;
        anzahlZeilen    =   zaehlerZeilen;
        anzahlZeichen   =   zaehlerZeichen;      
    }
}

/*** EINLESEN DES FILES ALS STRING, SPLIT ÜBER TOKEN UND ZEILENWEISE FORMATIERTE AUSGABE ÜBER CLI ***/
void fileReadCSVGetStringsCheckTokensPrintCLI()
{

    if(datenstrom != NULL)
    {

        /* ZAEHLER UND GRENZEN FESTLEGEN */

        //INITIALISIEREN GLOBARER VARIABLEN
        anzahlSpalten   = 12;       // FESTLEGUNG ZUR VEREINFACHUNG
        anzahlZeilen    = 1000;     // FESTLEGUNG ZUR VERREINFACHUNG
        anzahlZeichen   = 0;

        // ZAEHLER DEFINIEREN
        zaehlerSpalten   = 0;
        zaehlerZeilen    = 0;
        zaehlerToken    = 0;

        /* DATEN, ZEILEN UND REIHEN FESTLEGEN*/

        // (ATEN DEFINIEREN UND SPEICHER ZUWEISEN
        double **daten = (double **)malloc(anzahlZeilen * sizeof(double *));

        // INITIALISIEREN UND SPEICHERPLATZZUWEISUNG DER ZEILEN FUER SPALTENANZAHL
        for (zaehlerZeilen = 0; zaehlerZeilen < anzahlZeilen; zaehlerZeilen++)
        {

            // SPEICHERZUWEISUNG JE ZEILEN ENTSPRECHEND DER GROESSE UND ANZAHL DER SPALTEN
            daten[zaehlerZeilen] = (double *)malloc(anzahlSpalten*sizeof(double));
        }

        /* DATEN BEFUELLEN */

        // (ZEILEN)DATEN DINITIALISIREN UEBER DURCHLAUF DES GESAMTEN DATENSTROMS
        while (fgets(datenZeile, MAX_BUFFER_LINE, datenstrom) && (zaehlerZeilen<anzahlZeilen))
        {

            // DEFINITION UND INITIALISIERUNG TEMPORAERER CHAR
            char* tmp = strdup(datenZeile); // RUECKGABE POINTER AUF LEEREN 'NULL'-BYTE-CHAR (ZEILENUMBRUCH)

            // DEF UND INIT EINES KONSTANTEN POINTERS
            const char * tok;

            // DURCHLAUF DER ZEILEN INNERHALB DES DATENSTROMS
            for (tok = strtok(datenZeile, "\r"); tok && *tok; zaehlerToken++, tok = strtok(NULL, "\t\n"))
            {

            // EINGELESENEN STRING DEN SPALTEN DER DATEN ZUWEISEN
            daten[zaehlerSpalten][zaehlerToken] = atof(tok);

            // AUSGABE DES STRINGS
            printf("%f\t", daten[zaehlerSpalten][zaehlerToken]);
            }

        //ZEILENUMBRUCH ZUR DARTELLUNG
        printf("\n");

        // TEMPORAEREN SPEICHER FREIGEBEN
        free(tmp);

        // ZAEHLER DER ZEILEN HOCHSETZEN
        zaehlerZeilen++;
    
        // FORMATIERUNG ABSCHLUSS ASL OPTISCHE ABGRENZUNG
        printSeparation();
        }
    }
}

/*** EINLESEN DES FILES MIT SSCANF STRING, OHNE SPLIT UND AUSGABE ALS EIN DATENSATZ ÜBER CLI ***/
void fileReadCSVGetStringsCheckTokensPrintCLI_new(){

    // PUFFER DEFINIEREN
    char buffer[MAX_BUFFER_LINE];

    // ABFANG EINES BEI FEHLERNDER DATEI
    if (!datenstrom) {
            fprintf(stderr, "Can't open file.\n");
            exit(EXIT_FAILURE);
    }

    // EINLESEN DES STREAMS ALS STRING
    while (fgets(buffer, sizeof(buffer), datenstrom) != NULL) {

        if (sscanf(datenZeile, "%*lf,%*lf,%lf,%*lf,%lf,%*lf,%lf,%*lf,%lf,%*lf,%lf,%*lf,%lf", &datenstrom) != 1) {
                exit(EXIT_FAILURE);
        }
        printf("%s\n", datenZeile);

        if (++zaehlerZeilen >= 1000)
        break;

    }
}


/************************************************************************************************
*                                                                                               *
*                                        HAUPTPROGRAMM                                          *
*                                                                                               *
************************************************************************************************/

int main(int argc, char *argv[]) {

    /*** HEADER FESTLEGEN ***/
    setHeader();
    printHeader();

    /*** DATEIPFAD FESTLEGEN ***/
    fileDeclarePath();

    /*** OEFFNEN DER DATEI ***/
    fileOpen();

    /*** AUSLESEN DER DATEI***/
    //fileReadCharAndPrintToCLI();
    //fileReadCharCheckLinesAndPrintCLI();
    //fileReadCharCheckLinesPlusCommaAndPrintCLI();
    fileReadGetStringToArrayOfStruct();
    //fileReadCSVGetStringsCheckTokensPrintCLI();
    //fileReadCSVGetStringsCheckTokensPrintCLI_new();


    /*** AUSGABE DER METADATEN ***/
    printResult();

    /*** SCHLIESSEN DER DATEI ***/
    fileClose();
    fileFree();

    return 0;

}


